<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory,UsesUuid;

    protected $fillable = ['url'];
    protected $casts = ['meta_tags' => 'array', 'internal_links' => 'array'];
    public function website(){
        return $this->belongsTo(Pages::class);
    }

}
