<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    use HasFactory, UsesUuid;

    protected $casts = ['keywords' => 'array'];

    public function pages(){
        return $this->hasMany(Page::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function (Model $model) {
            dispatch( new \App\Jobs\CrawlWebsite($model));
        });
    }
}
