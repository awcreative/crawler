<?php

namespace App\Observers\Crawler;

use Exception;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver as SpatieCrawlObserver;
use Symfony\Component\DomCrawler\Crawler;

class ConsoleObserver extends SpatieCrawlObserver
{
    public function __construct(\Illuminate\Console\Command$console)
    {
        $this->console = $console;
    }

    /**
     * @param UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {
        $this->console->comment("Found: {$url}");
        //CrawlerQueue::firstOrCreate(['url' => $url]);
    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param UriInterface      $url
     * @param ResponseInterface $response
     * @param UriInterface|null $foundOnUrl
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null): void
    {
        $this->console->total_crawled++;
        // $this->console->info($response->getBody());
        //  $item = CrawlerQueue::onlyTrashed()->url($url)->first();

        //      $body = $response->getBody();
        //      if ($item->count()) {
        //          $item->html = $body;
        //          $item->meta_tags = json_encode($this->get_meta_tags(preg_replace('/\R+/', ' ', $body), $url));
        //          $item->save();
        //      }

        $this->console->info("Crawled: ({$this->console->total_crawled}) {$url} ({$foundOnUrl})");
    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param UriInterface      $url
     * @param RequestException  $requestException
     * @param UriInterface|null $foundOnUrl
     */
    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = null): void
    {
        $this->console->error("Fail: $url " . $requestException);
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        $this->console->info('Crawler: Finished');
    }

    public function get_meta_tags($html = null, $url = null)
    {
        try {
            $crawler = new Crawler($html);
            $meta    = $crawler->filterXPath('//head/meta[@name]')->each(function (Crawler $node, $i)
            {
                return [$node->attr('name') => $node->attr('content')];
            });
            $h1s = $crawler->filter('h1')->each(function (Crawler $node, $i)
            {
                return $node->text();
            });
            $h2s = $crawler->filter('h2')->each(function (Crawler $node, $i)
            {
                return $node->text();
            });
            //$canonical_link = $crawler->filterXPath('link[@rel="canonical"]')->first()->attr('href') ?? $url;
        }
        catch (\Exception$e)
        {
            $this->console->error($e);
            return ['error' => $e];
        }
        return ['meta' => $meta, 'h1' => $h1s, 'h2' => $h2s, 'cananoical' => ''];
    }

    public function isHTML($string)
    {
        return $string != strip_tags($string) ? true : false;
    }
}
