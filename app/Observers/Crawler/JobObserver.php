<?php

namespace App\Observers\Crawler;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;
use App\Models\CrawlerQueue;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Exception;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Facades\Http;
use Spatie\Crawler\CrawlObservers\CrawlObserver as SpatieCrawlObserver;
use Symfony\Component\DomCrawler\Crawler;


class JobObserver extends SpatieCrawlObserver
{

    public $internalLinks;
    public \App\Models\Website $website;
    protected mixed $baseUrl;

    public function __construct($website)
    {
        if (! $website->url instanceof UriInterface) {
            $baseUrl = new Uri($website->url);
        }

        $this->baseUrl = $baseUrl;
        $this->website = $website;
    }

    /**
     * @param UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {

    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param UriInterface $url
     * @param ResponseInterface $response
     * @param UriInterface|null $foundOnUrl
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = NULL): void
    {
            $item = CrawlerQueue::onlyTrashed()->url($url)->first();
            $body = $response->getBody();
            $page = new \App\Models\Page;
            $page->url = $item->url;
            $page->found_on_url = $foundOnUrl ? $foundOnUrl->__toString() : null;
            $page->html = $body;
            $page->path = parse_url($item->url, PHP_URL_PATH);
            $page->meta_tags = $this->get_meta_tags(preg_replace('/\R+/', ' ', $body), $item->url);
            $page->internal_links = $this->getInternalLinks(preg_replace('/\R+/', ' ', $body), $item->url);
            $this->website->pages()->save($page);
    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param UriInterface $url
     * @param RequestException $requestException
     * @param UriInterface|null $foundOnUrl
     */
    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = NULL): void
    {
        $item = CrawlerQueue::onlyTrashed()->url($url)->first();
        $item->error = $requestException->getMessage();
        $item->save();
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {

    }

    public function getInternalLinks($html = null, $pageurl = null){
        $crawler = new Crawler($html);
        $internal = [];
        $external = [];
        $links = $crawler->filter('a')->each(function (Crawler $node, $i) {
            return $node->attr('href');
        });
        foreach ($links as $link){
            if(parse_url($link,PHP_URL_HOST) == parse_url($pageurl,PHP_URL_HOST)){
                $internal[] = $link;
            }else{
                $external[] = $link;
            }
        }
        return compact('internal','external');
    }

    public function get_meta_tags($html = null, $pageurl = null)
    {
            $metas = [];
        try{


            $crawler = new Crawler($html);
            $responses = [];


                //Loop through the links array and perform a request to retive the additional data
                $title   = $crawler->filter('title')->text() ?? null;
                $meta    = $crawler->filterXPath('//head/meta[@name]')->each(function (Crawler $node, $i) {
                    return ['name' => $node->attr('name')?? null ,'value' =>  $node->attr('content') ?? null];
                });
                $h1_headings = $crawler->filter('h1')->each(function (Crawler $node, $i) {
                    return $node->text() ?? null;
                });
                $h2_headings = $crawler->filter('h2')->each(function (Crawler $node, $i) {
                    return $node->text() ?? null;
                });
                $headLinks = $crawler->filterXPath('//head/link[@rel]')->each(function (Crawler $node, $i) use($pool) {
                    return ['name' => $node->attr('rel') ?? 'unknown' ,'value' => $node->attr('href') ?? null];
                });
                $headScripts = $crawler->filterXPath('//head/script[@src]')->each(function (Crawler $node, $i) use($pool) {
                    return ['name' => $node->attr('type') ?? 'unknown' ,'value' => $node->attr('src') ?? null];
                });
                $meta_robots    = $metas['robots'] ?? null;
                $meta_desc      = $metas['description']  ?? null;
                $meta_keywords  = $metas['keywords']  ?? null;
                $head_links     = $headScripts;
                $head_scripts   = $headLinks;
                $canonical_link = $headLinks['canonical'] ?? null;

                foreach ($meta as $key => $mt){
                    $metas[$mt['name']] = $mt['value'];
                }
           }catch(\Exception $e){

            return ['error' => $metas];
        }
        return compact('pools','title','meta_robots','meta_desc','meta_keywords','canonical_link','h1_headings','h2_headings','head_links','head_scripts');
    }

    public function isHTML($string){
        return $string != strip_tags($string) ? true:false;
    }

}
