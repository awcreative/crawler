<?php

namespace App\Console\Commands;

use App\Observers\Crawler\ConsoleObserver;
use App\Queues\CrawlerCacheQueue;
use Spatie\Crawler\Crawler;
use Illuminate\Console\Command;
use Spatie\Browsershot\Browsershot;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;

class CrawlerRun extends Command
{
    public int $total_crawled = 0;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'craw {site}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepares and runs the crawler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $queue = null;
        $site = $this->argument('site');

        if (is_null($queue)) {
            $this->info('Preparing a new crawler queue');

            $queue = new CrawlerCacheQueue(86400); // one day
        }

        // Crawler
        $this->info('Start crawling');

        $run = (new Browsershot)
                ->setBinPath(app_path('Services/Browsershot/browser-local.js'))
                ->userAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

        $run->setNodeBinary('/opt/bin/node')
        ->setNodeModulePath('/opt/nodejs/node_modules')
        ->setBinPath(app_path('Services/Browsershot/browser-vapor.js'));

        Crawler::create()
            ->executeJavascript()
            ->setBrowsershot($run)
            ->setParseableMimeTypes(['text/html', 'text/plain'])
            ->addCrawlObserver(new ConsoleObserver($this))
            ->setConcurrency(20)
            ->setCrawlQueue($queue)
            ->setCrawlProfile(new CrawlInternalUrls($site))
            ->startCrawling($site);

        $this->alert("Crawled {$this->total_crawled} items");

        if ($queue->hasPendingUrls()) {
            $this->alert('Has URLs left');
        } else {
            $this->info('Has no URLs left');
        }

        return 0;
    }
}
