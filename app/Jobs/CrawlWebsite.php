<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Observers\Crawler\JobObserver;
use App\Queues\CrawlerCacheQueue;
use Spatie\Crawler\Crawler;
use Spatie\Browsershot\Browsershot;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;


class CrawlWebsite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $site;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($site)
    {
        $this->site = $site;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $queue = null;
        $site = $this->site;

        if (is_null($queue)) {
            $queue = new CrawlerCacheQueue(86400); // one day
        }

        $run = (new Browsershot)
        ->userAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
        ->setNodeBinary('/opt/bin/node')
        ->setNodeModulePath('/opt/nodejs/node_modules')
        ->setBinPath(app_path('Services/Browsershot/browser-vapor.js'));

        Crawler::create()
            ->executeJavascript()
            ->setBrowsershot($run)
            ->setParseableMimeTypes(['text/html', 'text/plain'])
            ->addCrawlObserver(new JobObserver($site))
            ->setConcurrency(5)
            ->setTotalCrawlLimit(500)
            ->setCurrentCrawlLimit(5)
            ->setCrawlQueue($queue)
            ->setCrawlProfile(new CrawlInternalUrls($site->url))
            ->startCrawling($site->url);

        if ($queue->hasPendingUrls()) {
            dispatch(new CrawlWebsite($site));
        }else{
            $site->crawl_complete = true;
            $site->save();
        }
    }
}
