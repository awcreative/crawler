<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Observers\Crawler\ConsoleObserver;;
use App\Observers\Crawler\JobObserver;
use App\Queues\CrawlerCacheQueue;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;
use Spatie\Browsershot\Browsershot;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Image\Manipulations;
use vipnytt\SitemapParser;
use vipnytt\SitemapParser\Exceptions\SitemapParserException;


class CrawlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSitemap(Request $request){
        try {
            $parser = new SitemapParser('Mozilla');
            $parser->parseRecursive($request->url);
            $response = [];
            foreach ($parser->getSitemaps() as $url => $tags) {
                $unternalUrls = [];
                $internalparser = new SitemapParser('Mozilla');
                $internalparser->parse($url);
                foreach ($internalparser ->getURLs() as $url => $tags) {
                    $unternalUrls[] = $url;
                }
                $response['indices'][$url] = $unternalUrls;
            }

            foreach ($parser->getURLs() as $url => $tags) {
               $response['urls'][] = $url;
            }
        } catch (SitemapParserException $e) {
            echo $e->getMessage();
        }
        return response()->json($response);
    }

    public function getSingleCrawl(Request $request){

        $site = $request->url;

        $queue = null;

        if (is_null($queue)) {
            $queue = new CrawlerCacheQueue(86400); // one day
        }

        $run = (new Browsershot)
        ->userAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
        ->setNodeBinary('/opt/bin/node')
        ->setNodeModulePath('/opt/nodejs/node_modules')
        ->setBinPath(app_path('Services/Browsershot/browser-vapor.js'));

        Crawler::create()
            ->executeJavascript()
            ->setBrowsershot($run)
            ->setParseableMimeTypes(['text/html', 'text/plain'])
            ->addCrawlObserver(new JobObserver($site))
            ->setTotalCrawlLimit(1)
            ->setCurrentCrawlLimit(1)
            ->setCrawlQueue($queue)
            ->setCrawlProfile(new CrawlInternalUrls($site))
            ->startCrawling($site);

        if ($queue->hasPendingUrls()) {
            $item = \App\Models\CrawlerQueue::onlyTrashed()->url($site)->first();
        }else{
            $item = \App\Models\CrawlerQueue::onlyTrashed()->url($site)->first();
        }

        return response()->json(array_merge([
            'url' => $site,
            'internal_links' => $item->internal_links['internal'],
            'external_links' => $item->internal_links['external']
        ],$item->meta_tags));
    }

    public function getScreenshot(Request $request){

        $domain = parse_url($request->url);

        if(Storage::exists('screenshots/'.$domain['host'].'.jpg')) Storage::delete('screenshots/'.$domain['host'].'.jpg');
        BrowserShot::url($request->url)
        ->userAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
        ->setNodeBinary('/opt/bin/node')
        ->setNodeModulePath('/opt/nodejs/node_modules')
        ->setBinPath(app_path('Services/Browsershot/browser-vapor.js'))
        ->waitUntilNetworkIdle()
        ->windowSize(1920, 1080)
        ->save('/mnt/local/screenshot'.$domain['host'].'.jpg');

        $path = Storage::putFileAs('screenshots', new File('/mnt/local/screenshot'.$domain['host'].'.jpg'), $domain['host'].'.jpg','public');
        unlink('/mnt/local/screenshot'.$domain['host'].'.jpg');
        return response()->json(['url' => Storage::url($path)], 200);

    }
}
