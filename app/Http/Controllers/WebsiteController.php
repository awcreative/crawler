<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use \App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\WebsiteResource;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = [
             "simple_url" => 'required|string',
             "url" => 'required:string|unique:websites,url'
         ];

         $validator = Validator::make($request->all(), $rules);

         if ($validator->fails()) {

         }

        $website = new Website();
        $website->forceFill([
            "simple_url"    => $request->simple_url,
            "url"           => $request->url,
            "ignore_robots" => $request->ignore_robots,
            "custom_robots" => $request->custom_robots,
            "waitUntil"     => $request->waitUntil,
            "proxyBypass"   => $request->proxyBypass,
            "keywords"      => $request->keywords
        ])->save();
        return new WebsiteResource($website);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $website = \App\Models\Website::find($id);
       $website->load('pages');
       return new WebsiteResource($website);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
