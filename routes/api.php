<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrawlController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function(){
    Route::apiResource('crawl', CrawlController::class);
    Route::get('singleCrawl',[CrawlController::class , 'getSingleCrawl']);
    Route::get('multipleCrawl',[CrawlController::class , 'getMultiCrawl']);
    Route::get('content',[CrawlController::class , 'getContent']);
    Route::get('siteMaps',[CrawlController::class , 'getSitemap']);
    Route::post('screenshots',[CrawlController::class , 'getScreenshot']);
    Route::apiResource('pages', PageController::class);
    Route::get('pages/related/{id}',[PageController::class, 'getRelated']);
    Route::get('pages/linked',[PageController::class, 'getLinked']);
    Route::delete('pages/related',[PageController::class, 'deleteRelated']);
    Route::apiResource('websites', WebsiteController::class);
});

