<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('url')->unique();
            $table->string('found_on_url')->nullable();
            $table->foreignUuid('website_id')->onDelete('cascade');
            $table->string('path');
            $table->longText('html')->nullable()->collation('utf8mb4_bin');
            $table->longText('meta_tags')->nullable()->collation('utf8mb4_bin');
            $table->longText('internal_links')->nullable()->collation('utf8mb4_bin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
