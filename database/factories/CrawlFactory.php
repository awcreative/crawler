<?php

namespace Database\Factories;

use App\Models\Crawl;
use Illuminate\Database\Eloquent\Factories\Factory;

class CrawlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Crawl::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
